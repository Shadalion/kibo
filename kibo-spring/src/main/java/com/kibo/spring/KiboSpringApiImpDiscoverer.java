package com.kibo.spring;

import com.kibo.server.api.KiboApiImpl;
import com.kibo.server.api.KiboApiImplDiscoverer;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Collection;

public class KiboSpringApiImpDiscoverer implements KiboApiImplDiscoverer, ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public Collection<Object> discover() {
        return applicationContext.getBeansWithAnnotation(KiboApiImpl.class).values();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
