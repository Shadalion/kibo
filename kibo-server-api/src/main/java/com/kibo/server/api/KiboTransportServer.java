package com.kibo.server.api;

public interface KiboTransportServer {
    <T extends KiboTransportServerConfig> void setConfig(T transportServerConfig);
    void start();
    void stop();
}
