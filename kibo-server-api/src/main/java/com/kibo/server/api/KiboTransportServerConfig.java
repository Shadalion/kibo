package com.kibo.server.api;

public interface KiboTransportServerConfig {
    KiboApiImplDiscoverer apiImplDiscoverer();
    int workersPoolSize();
}
