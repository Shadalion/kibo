package com.kibo.server.api;

import java.util.Collection;

public interface KiboApiImplDiscoverer {
    Collection<Object> discover();
}
