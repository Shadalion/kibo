package com.kibo.zeromq.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.UUID;

public class KiboZeromqRqProxy implements InvocationHandler{
    private ZContext ctx;
    private String address;
    private Class classToInvoke;
    private ObjectMapper mapper;

    private static final ThreadLocal<ZMQ.Socket> socket = ThreadLocal.withInitial(() -> null);

    public KiboZeromqRqProxy(ZContext ctx,
                             Class classToInvoke,
                             String address) {
        this.ctx = ctx;
        this.classToInvoke = classToInvoke;
        this.address = address;
        this.mapper = new ObjectMapper();
    }

    @Override
    public Object invoke(Object proxy, Method methodToInvoke, Object[] args) throws JsonProcessingException {
        ZMQ.Socket ls = socket.get();

        if(ls == null) {
            ls = ctx.createSocket(SocketType.REQ);
            ls.connect(address);
        }

        ls.setIdentity(UUID.randomUUID().toString().getBytes());

        ls.sendMore(concatHeader(classToInvoke, methodToInvoke));

        for(int i = 0; i < args.length - 1; i++) {
            ls.sendMore(mapper.writeValueAsString(args[0]));
        }

        ls.send(mapper.writeValueAsString(args[args.length - 1]));

        String status = ls.recvStr();

        if(status.contains("error")) {
            throw new RuntimeException("Error");
        }

        return ls.recvStr();
    }

    private String concatHeader(Class wrappedClazz,
                                Method method) {
        StringBuilder sb = new StringBuilder();
        sb.append(wrappedClazz.getName());
        sb.append("#");
        sb.append(method.getName());
        sb.append("#");

        for(Class clazz : method.getParameterTypes()) {
            sb.append(clazz.getName());
            sb.append("#");
        }

        return sb.toString();
    }
}
