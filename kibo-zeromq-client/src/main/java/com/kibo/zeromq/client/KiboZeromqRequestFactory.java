package com.kibo.zeromq.client;

import org.zeromq.ZContext;

import java.lang.reflect.Proxy;

public class KiboZeromqRequestFactory {

    public <T> T createTransportProxy(Class<T> clazz, String address) {
        ZContext context = new ZContext();

        Runtime.getRuntime().addShutdownHook(new Thread(context::close));

        return (T) Proxy.newProxyInstance(clazz.getClassLoader(),
                new Class[] {clazz},
                new KiboZeromqRqProxy(context, clazz, address));

    }
}
