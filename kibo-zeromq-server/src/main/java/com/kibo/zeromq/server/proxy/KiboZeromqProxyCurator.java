package com.kibo.zeromq.server.proxy;

import com.kibo.zeromq.server.config.KiboZeromqConfig;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

public class KiboZeromqProxyCurator {

    private ZMQ.Socket router;
    private ZMQ.Socket dealer;
    private ZMQ.Socket controlRep;
    private ZMQ.Socket controlReq;

    public void start(ZContext ctx,
                      KiboZeromqConfig config) {
        router = ctx.createSocket(SocketType.ROUTER);

        String inboundAddress = "tcp://localhost:" + config.routerPort();

        if (!router.bind(inboundAddress)) {
            throw new RuntimeException("Can't bind socket to " + inboundAddress);
        }

        dealer = ctx.createSocket(SocketType.DEALER);

        if(!dealer.bind("inproc://kiboworkers")) {
            throw new RuntimeException("Cannot connect dealer socket to inproc");
        }

        controlRep = ctx.createSocket(SocketType.REP);
        controlRep.bind("inproc://kibopzmqroxycontrol");

        controlReq = ctx.createSocket(SocketType.REQ);
        controlReq.connect("inproc://kibopzmqroxycontrol");

        new Thread(() -> config.innerProxyFunction().proxy(router, dealer, null, controlRep),
                "kibo-server-proxy-thread").start();
    }

    public void dispose() {
        if(controlReq != null) {
            controlReq.send("TERMINATE");
            controlReq.close();
        }

        if(controlRep != null) {
            controlRep.close();
        }

        if(router != null) {
            router.close();
        }

        if(dealer != null) {
            dealer.close();
        }
    }
}
