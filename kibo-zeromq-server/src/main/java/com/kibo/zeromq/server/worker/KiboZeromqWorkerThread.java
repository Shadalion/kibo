package com.kibo.zeromq.server.worker;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.util.Arrays;
import java.util.Map;

public class KiboZeromqWorkerThread extends Thread {

    private static final Logger LOGGER = LoggerFactory.getLogger(KiboZeromqWorkerThread.class);

    private Map<Class, Object> services;
    private ObjectMapper objectMapper;

    private ZMQ.Socket worker;
    private ZMQ.Socket control;
    private ZMQ.Poller poller;

    public KiboZeromqWorkerThread(ZContext ctx,
                                  Map<Class, Object> services,
                                  String threadName) {
        super(threadName);
        this.objectMapper = new ObjectMapper();
        this.services = services;
        this.worker = ctx.createSocket(SocketType.DEALER);

        if(!this.worker.connect("inproc://kiboworkers")) {
            throw new RuntimeException("Can't connect worker socket to inproc://kiboworkers");
        }

        this.control = ctx.createSocket(SocketType.SUB);

        if(!this.control.connect("inproc://kiboworkerssub")) {
            throw new RuntimeException("Can't connect control socket to inproc://kiboworkerssub");
        }
        this.control.subscribe("TERMINATE".getBytes(ZMQ.CHARSET));

        this.poller = ctx.createPoller(2);
        this.poller.register(worker, ZMQ.Poller.POLLIN);
        this.poller.register(control, ZMQ.Poller.POLLIN);
    }

    @Override
    public void run() {
        try {
            main: while (!Thread.interrupted()) {
                poller.poll();

                if (poller.pollin(0)) {
                    String identity = worker.recvStr();
                    worker.recvStr();
                    String transportHeaders = worker.recvStr();
                    String[] transportHeadersArray = transportHeaders.split("#");
                    //Class#methodName#Arg1#arg2
                    int numberOfParams = transportHeadersArray.length - 2;
                    Object[] args = new Object[numberOfParams];
                    Class[] argsClazz = new Class[numberOfParams];

                    for (int i = 0; i < numberOfParams; i++) {
                        try {
                            argsClazz[i] = Class.forName(transportHeadersArray[i + 2]);
                            args[i] = objectMapper.readValue(worker.recvStr(), argsClazz[i]);
                        } catch (Exception e) {
                            LOGGER.error("ERROR while parse request {}", transportHeaders, e);
                            worker.sendMore(identity);
                            worker.sendMore("");
                            worker.send("status:error");
                            continue main;
                        }
                    }

                    try {
                        Object objectOnInvoke = services.get(Class.forName(transportHeadersArray[0]));
                        Object callResult = objectOnInvoke.getClass().getMethod(transportHeadersArray[1], argsClazz)
                                .invoke(objectOnInvoke, args);
                        worker.sendMore(identity);
                        worker.sendMore("");
                        worker.sendMore("status:success");
                        worker.send(objectMapper.writeValueAsString(callResult));
                    } catch (Exception e) {
                        LOGGER.error("Error while call logic, call params {}, threadName {}", Arrays.toString(transportHeadersArray),
                                Thread.currentThread().getName(), e);
                    }
                } else if (poller.pollin(1)) {
                    worker.close();
                    control.close();
                    poller.close();
                    break;
                }
            }
        } catch (Throwable e) {
            LOGGER.error("Unexpected error has occured in working thread {}", Thread.currentThread().getName(), e);
        } finally {
            Thread.currentThread().interrupt();
            try {
                worker.close();
                control.close();
                poller.close();
            } catch (Throwable e) {
                LOGGER.error("Can't close resources in worker thread {}", Thread.currentThread().getName(), e);
            }
        }
    }
}
