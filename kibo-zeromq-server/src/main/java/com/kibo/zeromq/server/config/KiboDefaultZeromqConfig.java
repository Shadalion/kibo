package com.kibo.zeromq.server.config;

import com.kibo.server.api.KiboApiImplDiscoverer;
import org.zeromq.ZMQ;

public class KiboDefaultZeromqConfig implements KiboZeromqConfig {
    private int workersSize;
    private int port;
    private KiboApiImplDiscoverer kiboApiImplDiscoverer;

    public KiboDefaultZeromqConfig(int workersSize,
                                   int port,
                                   KiboApiImplDiscoverer kiboApiImplDiscoverer) {
        this.workersSize = workersSize;
        this.port = port;
        this.kiboApiImplDiscoverer = kiboApiImplDiscoverer;
    }

    @Override
    public KiboZeromqInnerProxy innerProxyFunction() {
        return ZMQ::proxy;
    }

    @Override
    public int routerPort() {
        return port;
    }

    @Override
    public int workersPoolSize() {
        return workersSize;
    }

    @Override
    public KiboApiImplDiscoverer apiImplDiscoverer() {
        return kiboApiImplDiscoverer;
    }
}

