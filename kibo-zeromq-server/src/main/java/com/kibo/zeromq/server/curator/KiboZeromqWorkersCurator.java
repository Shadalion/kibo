package com.kibo.zeromq.server.curator;

import com.kibo.client.api.KiboApi;
import com.kibo.zeromq.server.config.KiboZeromqConfig;
import com.kibo.zeromq.server.worker.KiboZeromqWorkerThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.util.*;
import java.util.stream.Collectors;

public class KiboZeromqWorkersCurator {
    private static final Logger LOGGER = LoggerFactory.getLogger(KiboZeromqWorkersCurator.class);

    private ZMQ.Socket control;

    public KiboZeromqWorkersCurator() {}

    public void start(ZContext zctx,
                      KiboZeromqConfig config) {
        Collection<Object> services = config.apiImplDiscoverer().discover();

        if (services.isEmpty()) {
            LOGGER.error("Can't find kibo api implementations through discoverer " + config.apiImplDiscoverer());
        }

        Map<Class, Object> servicesCache = services.stream().collect(
                Collectors.toMap(
                        obj -> Arrays.stream(obj.getClass().getInterfaces())
                                .filter(interfaceClazz -> interfaceClazz.isAnnotationPresent(KiboApi.class))
                                .findFirst()
                                .orElseThrow(() -> new RuntimeException("Discoverer present api impl without com.kibo.annotations.KiboApi interface")),
                        s -> s)
        );

        for (int i = 0; i < config.workersPoolSize(); i++) {
            KiboZeromqWorkerThread worker = new KiboZeromqWorkerThread(zctx,
                    servicesCache, "kibo-zmq-worker-thread-" + i);
            worker.start();
        }

        control = zctx.createSocket(SocketType.PUB);

        if(!control.bind("inproc://kiboworkerssub")) {
            throw new RuntimeException("Can't bind control curator socket to inproc://kiboworkerssub");
        }
    }

    public void dispose() {
        if(control != null) {
            control.send("TERMINATE");
            control.close();
        }
    }
}
