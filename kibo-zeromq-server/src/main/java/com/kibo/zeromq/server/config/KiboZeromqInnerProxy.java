package com.kibo.zeromq.server.config;

import org.zeromq.ZMQ;

public interface KiboZeromqInnerProxy {
    boolean proxy(ZMQ.Socket frontend, ZMQ.Socket backend, ZMQ.Socket capture, ZMQ.Socket control);
}
