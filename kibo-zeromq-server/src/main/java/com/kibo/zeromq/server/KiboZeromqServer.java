package com.kibo.zeromq.server;

import com.kibo.server.api.KiboTransportServer;
import com.kibo.server.api.KiboTransportServerConfig;
import com.kibo.zeromq.server.config.KiboZeromqConfig;
import com.kibo.zeromq.server.curator.KiboZeromqWorkersCurator;
import com.kibo.zeromq.server.proxy.KiboZeromqProxyCurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZContext;

public class KiboZeromqServer implements KiboTransportServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(KiboZeromqServer.class);

    private KiboZeromqConfig kiboZeromqConfig;
    private ZContext context;

    private KiboZeromqWorkersCurator workersCurator;
    private KiboZeromqProxyCurator proxyCurator;

    @Override
    public <T extends KiboTransportServerConfig> void setConfig(T transportServerConfig) {
        this.kiboZeromqConfig = (KiboZeromqConfig) transportServerConfig;
    }

    @Override
    public void start() {
        if (context != null) {
            LOGGER.error("ZContext already started");
            return;
        }

        context = new ZContext();

        //Runtime.getRuntime().addShutdownHook();
        try {
            proxyCurator = new KiboZeromqProxyCurator();
            proxyCurator.start(context, kiboZeromqConfig);
        } catch (Exception e) {
            LOGGER.error("Can't start proxy, try to dispose server", e);
            stop();
        }

        try {
            workersCurator = new KiboZeromqWorkersCurator();
            workersCurator.start(context, kiboZeromqConfig);
        } catch (Exception e) {
            LOGGER.error("Can't start workers, try to dispose server", e);
            stop();
        }
    }

    @Override
    public void stop() {
        try {
            try {
                proxyCurator.dispose();
            } catch (Throwable e) {
                LOGGER.error("Can't properly dispose proxy", e);
            }

            try {
                workersCurator.dispose();
            } catch (Throwable e) {
                LOGGER.error("Can't properly dispose workers", e);
            }
        } finally {
            try {
                context.close();
            } catch (Throwable e) {
                LOGGER.error("Can't properly close zeromq context", e);
            }
        }
    }
}
