package com.kibo.zeromq.server.config;

import com.kibo.server.api.KiboTransportServerConfig;

public interface KiboZeromqConfig extends KiboTransportServerConfig {
    KiboZeromqInnerProxy innerProxyFunction();
    int routerPort();
}
