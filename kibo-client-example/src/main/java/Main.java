import com.example.KiboInterfaceExample;
import com.kibo.zeromq.client.KiboZeromqRequestFactory;
import com.kibo.zeromq.server.KiboZeromqServer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;


public class Main {
    public static void main(String[] args) throws InterruptedException {
        KiboZeromqRequestFactory factory = new KiboZeromqRequestFactory();
        KiboInterfaceExample request = factory.createTransportProxy(KiboInterfaceExample.class, "tcp://localhost:5555");

        ApplicationContext ctx = new GenericXmlApplicationContext("ctx.xml");
        KiboZeromqServer server = (KiboZeromqServer) ctx.getBean("server");
        server.start();

        String response = request.exampleMethod("PARAMETER", 5L);

        server.stop();
    }
}
