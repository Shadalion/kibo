package com.example;

import com.kibo.server.api.KiboApiImpl;

@KiboApiImpl
public class KiboServiceExample implements KiboInterfaceExample {

    @Override
    public String exampleMethod(String parameter, Long parameter2) {
        return parameter + parameter2;
    }
}
