package com.example;

import com.kibo.client.api.KiboApi;

@KiboApi
public interface KiboInterfaceExample {
    String exampleMethod(String parameter, Long parameter2);
}
